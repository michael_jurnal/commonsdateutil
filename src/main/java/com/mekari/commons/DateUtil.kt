package com.mekari.commons

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {
    fun Calendar.normalizeTime(): Calendar {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)

        return this
    }

    fun Date?.isToday(): Boolean {
        if (this == null) return false

        val compareCal = Calendar.getInstance().apply {
            time = this@isToday
            normalizeTime()
        }

        val todayCal = Calendar.getInstance().normalizeTime()

        return todayCal.compareTo(compareCal) == 0
    }

    fun Date?.isDayBefore(): Boolean {
        if (this == null) return false

        val compareCal = Calendar.getInstance().apply {
            time = this@isDayBefore
            normalizeTime()
        }

        val todayCal = Calendar.getInstance().normalizeTime()

        return compareCal.before(todayCal)
    }

    fun Date?.isDayAfter(): Boolean {
        if (this == null) return false

        val compareCal = Calendar.getInstance().apply {
            time = this@isDayAfter
            normalizeTime()
        }

        val todayCal = Calendar.getInstance().normalizeTime()

        return compareCal.after(todayCal)
    }

    fun Date?.add(day: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.time = this
        calendar.add(Calendar.DATE, day)
        return calendar.time
    }

    fun Date?.format(format: DateFormat, locale: Locale = Locale.getDefault()): String =
            this?.let { SimpleDateFormat(format.value, locale).format(it) }.orEmpty()

    fun parseDate(dateString: String?, format: DateFormat, locale: Locale = Locale.getDefault()): Date? {
        return try {
            SimpleDateFormat(format.value, locale).parse(dateString)
        } catch (pe: ParseException) {
            null
        }
    }

    fun changeFormat(dateString: String?, oldFormat: DateFormat, newFormat: DateFormat): String? {
        val date = parseDate(dateString, oldFormat) ?: return dateString
        return date.format(newFormat)
    }

    fun parseCalendar(dateString: String, format: DateFormat = DateFormat.ISO_8601): Calendar? {
        val date = parseDate(dateString, format) ?: return null
        return Calendar.getInstance().apply { time = date }
    }
}