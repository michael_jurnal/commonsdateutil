package com.mekari.commons

enum class DateFormat(val value: String) {
    DAY_OF_MONTH("dd"),
    DAY_NAME_IN_WEEK("EEE"),
    FULL_DAY_NAME_IN_WEEK("EEEE"),
    DATE_DAY("EEE dd"),
    DAY_MONTH("dd MMM"),
    DEFAULT("dd MMM yyyy"),
    MONTH_YEAR("MMMM yyyy"),
    PERIOD("yyyyMM"),
    FULL_DATE("EEE, dd MMM yyyy"),
    LOCAL_TIME("HH:mm:ss"),
    HOUR_MINUTE("HH:mm"),
    LOCAL_DATE_TIME("yyyy-MM-dd HH:mm:ss"),
    LOCAL_DATE("yyyy-MM-dd"),
    ISO_8601("yyyy-MM-dd'T'HH:mm:ss'Z'"),
    DATE_TIME("dd MMM yyyy, HH:mm:ss"),
    DATE_HOUR_MINUTE("dd MMM yyyy, HH:mm")
}