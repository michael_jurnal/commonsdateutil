package com.mekari.commons

import org.junit.Test
import com.mekari.commons.DateUtil.isToday
import com.mekari.commons.DateUtil.add
import com.mekari.commons.DateUtil.isDayBefore
import com.mekari.commons.DateUtil.isDayAfter
import com.mekari.commons.DateUtil.format
import org.junit.Assert.*
import java.util.*

class DateUtilTest {

    private val defaultDate: Date

    init {
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(1993, Calendar.SEPTEMBER, 15, 11, 11, 11)
        calendar.set(Calendar.MILLISECOND, 0)
        defaultDate = calendar.time
    }

    @Test
    fun test_IsToday_givenDateToday_shouldReturnTrue() {
        val currentDate = Date()
        assertTrue(currentDate.isToday())
    }

    @Test
    fun test_IsToday_givenDateYesterday_shouldReturnFalse() {
        val yesterday = Date().add(1)
        assertFalse(yesterday.isToday())
    }

    @Test
    fun test_isDayBefore_givenDateToday_shouldReturnFalse() {
        val today = Date()
        assertFalse(today.isDayBefore())
    }

    @Test
    fun test_isDayBefore_givenDateYesterday_shouldReturnTrue() {
        val yesterday = Date().add(-1)
        assertTrue(yesterday.isDayBefore())
    }

    @Test
    fun test_isDayAfter_givenDateToday_shouldReturnFalse() {
        val today = Date()
        assertFalse(today.isDayAfter())
    }

    @Test
    fun test_isDayAfter_givenDateTomorrow_shouldReturnTrue() {
        val tomorrow = Date().add(1)
        assertTrue(tomorrow.isDayAfter())
    }

    @Test
    fun test_format_withDateFormatFullDate() {
        val actual = defaultDate.format(DateFormat.FULL_DATE)
        val expected = "Wed, 15 Sep 1993"

        assertEquals(expected, actual)
    }

    @Test
    fun test_parseDate_giveDateWithDateFormatISO8601_shouldReturnDate() {
        val input = "1993-09-15T11:11:11Z"
        val actual = DateUtil.parseDate(input, DateFormat.ISO_8601)
        val expected = defaultDate

        assertEquals(expected, actual)
    }

    @Test
    fun test_changeFormat_givenDateWithOldFormatFullDateAndNewFormatISO8601_shouldReturnDateFormatISO8601() {
        val expected = "1993-09-15T00:00:00Z"
        val input = "Wed, 15 Sep 1993"
        val actual = DateUtil.changeFormat(input, oldFormat = DateFormat.FULL_DATE, newFormat = DateFormat.ISO_8601)

        assertEquals(expected, actual)
    }

    @Test
    fun test_parseCalendar_givenDateStringWithDateFormatISO8601_shouldReturnCalendar() {
        val expected = Calendar.getInstance()
        expected.time = defaultDate

        val input = "1993-09-15T11:11:11Z"
        val actual = DateUtil.parseCalendar(input, DateFormat.ISO_8601)
        assertEquals(expected, actual)
    }
}